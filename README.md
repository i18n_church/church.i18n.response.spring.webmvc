# Spring WebMVC REST error handling

Extension of `church.i18n.response.spring` library for Spring Web MVC projects. It adds handling of
most common Spring WebMVC Exceptions.

**SpringWebMvcRequestExceptionResolver.class**
Handles following exceptions

* BindException.class
* MethodArgumentNotValidException.class
* NoHandlerFoundException.class

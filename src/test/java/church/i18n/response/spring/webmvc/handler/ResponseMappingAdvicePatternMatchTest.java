/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.webmvc.handler;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.Response;
import church.i18n.response.handler.GenericExceptionHandler;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import church.i18n.response.spring.mapper.ResponseEntityResponseDataMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

class ResponseMappingAdvicePatternMatchTest {

  private final ProcessingIdProvider pid = () -> "main";
  private final BaseResponseMapper mapper = BaseResponseMapper.builder()
      .withBundle(new PolyglotMultiResourceBundle(GenericExceptionHandler.RESOURCE_BUNDLE_NAME))
      .withExposeSanitizer((infoSecurityPolicy, config) -> true)
      .withProcessingIdProvider(pid)
      .build();

  private final MockMvc mvcMock = MockMvcBuilders
      .standaloneSetup(new TestingController())
      .setControllerAdvice(
          new TestSpringWebMvcRequestExceptionResolverControllerAdvice(),
          new ResponseMappingAdvice(mapper)
      )
      .setMessageConverters(new MappingJackson2HttpMessageConverter(new ObjectMapper()))
      .build();

  @ParameterizedTest
  @MethodSource("matchesApiEndpoint")
  void matchesApiEndpoint(final String endpoint, final String response) throws Exception {
    MockMvcBuilders
        .standaloneSetup(new TestingController())
        .setControllerAdvice(
            new TestSpringWebMvcRequestExceptionResolverControllerAdvice(),
            new ResponseMappingAdvice(mapper, "/api")
        )
        .setMessageConverters(new MappingJackson2HttpMessageConverter(new ObjectMapper()))
        .build()
        .perform(MockMvcRequestBuilders.get(endpoint).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().string(response));
  }

  static Stream<Arguments> matchesApiEndpoint() {
    return Stream.of(
        Arguments.of("/api", "1"),
        Arguments.of("/api-docs",
            "{\"requestId\":\"main\",\"data\":\"{\\\"pretend\\\": \\\"JSON\\\"}\",\"error\":null,\"messages\":[]}"),
        Arguments.of("/api/v1", "{\"requestId\":\"main\",\"data\":127,\"error\":null,\"messages\":[]}"),
        Arguments.of("/endpoint", "{\"requestId\":\"main\",\"data\":32767,\"error\":null,\"messages\":[]}")
    );
  }

  @ParameterizedTest
  @MethodSource("matchesStartingWithApiEndpoint")
  void matchesStartingWithApiEndpoint(final String endpoint, final String response) throws Exception {
    MockMvcBuilders
        .standaloneSetup(new TestingController())
        .setControllerAdvice(
            new TestSpringWebMvcRequestExceptionResolverControllerAdvice(),
            new ResponseMappingAdvice(mapper, "/api*")
        )
        .setMessageConverters(new MappingJackson2HttpMessageConverter(new ObjectMapper()))
        .build()
        .perform(MockMvcRequestBuilders.get(endpoint).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().string(response));
  }

  static Stream<Arguments> matchesStartingWithApiEndpoint() {
    return Stream.of(
        Arguments.of("/api", "1"),
        Arguments.of("/api-docs", "\"{\\\"pretend\\\": \\\"JSON\\\"}\""),
        Arguments.of("/api/v1", "{\"requestId\":\"main\",\"data\":127,\"error\":null,\"messages\":[]}"),
        Arguments.of("/endpoint", "{\"requestId\":\"main\",\"data\":32767,\"error\":null,\"messages\":[]}")
    );
  }

  @ParameterizedTest
  @MethodSource("matchesAnythingStartingWithApi")
  void matchesAnythingStartingWithApi(final String endpoint, final String response) throws Exception {
    MockMvcBuilders
        .standaloneSetup(new TestingController())
        .setControllerAdvice(
            new TestSpringWebMvcRequestExceptionResolverControllerAdvice(),
            new ResponseMappingAdvice(mapper, "/api/**", "/api*")
        )
        .setMessageConverters(new MappingJackson2HttpMessageConverter(new ObjectMapper()))
        .build()
        .perform(MockMvcRequestBuilders.get(endpoint).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().string(response));
  }

  static Stream<Arguments> matchesAnythingStartingWithApi() {
    return Stream.of(
        Arguments.of("/api", "1"),
        Arguments.of("/api-docs", "\"{\\\"pretend\\\": \\\"JSON\\\"}\""),
        Arguments.of("/api/v1", "127"),
        Arguments.of("/endpoint", "{\"requestId\":\"main\",\"data\":32767,\"error\":null,\"messages\":[]}")
    );
  }

  @RestController
  public static class TestingController {

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/api-docs")
    public String apiDocs() {
      return "{\"pretend\": \"JSON\"}";
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/api")
    public int intHandler() {
      return 1;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/api/v1")
    public byte byteHandler() {
      return 127;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/endpoint")
    public short shortHandler() {
      return 32767;
    }
  }

  @ControllerAdvice
  private class TestSpringWebMvcRequestExceptionResolverControllerAdvice implements
      GenericExceptionHandler<String> {

    private final ThreadLocalStorage storage = new ThreadLocalStorage(pid);

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return mapper;
    }

    @Override
    public LogMapper getLogMapper() {
      return mapper.getLogMapper();
    }

    @Override
    public MessageStorage getMessageStorage() {
      return storage;
    }
  }

  private static class TestHttpResponseMapper implements ResponseEntityResponseDataMapper {

    private final BaseResponseMapper mapper;
    private final ThreadLocalStorage storage;

    public TestHttpResponseMapper(final BaseResponseMapper mapper) {
      this.mapper = mapper;
      this.storage = new ThreadLocalStorage(() -> "thread-id-provider");
    }

    @Override
    public LogMapper getLogMapper() {
      return mapper.getLogMapper();
    }
    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return this.mapper;
    }

    @Override
    public MessageStorage getMessageStorage() {
      return this.storage;
    }
  }
}


/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.webmvc.handler;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.Response;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.handler.GenericExceptionHandler;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import church.i18n.response.spring.mapper.ResponseEntityResponseDataMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

class ResponseMappingAdviceTest {

  private final ProcessingIdProvider pid = () -> "main";
  private final BaseResponseMapper mapper = BaseResponseMapper.builder()
      .withBundle(new PolyglotMultiResourceBundle(GenericExceptionHandler.RESOURCE_BUNDLE_NAME))
      .withExposeSanitizer((infoSecurityPolicy, config) -> true)
      .withProcessingIdProvider(pid)
      .build();

  private final MockMvc mvcMock = MockMvcBuilders
      .standaloneSetup(new TestingController(), new TestingExceptionController())
      .setControllerAdvice(
          new TestSpringWebMvcRequestExceptionResolverControllerAdvice(),
          new ResponseMappingAdvice(mapper)
      )
      .setMessageConverters(new MappingJackson2HttpMessageConverter(new ObjectMapper()))
      .build();

  @ParameterizedTest
  @MethodSource("dataEndpoints")
  void testEndpoint(final String endpoint, final String response) throws Exception {
    mvcMock.perform(MockMvcRequestBuilders.get(endpoint).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().string(response));
  }


  @ParameterizedTest
  @MethodSource("exceptionEndpoints")
  void exceptionEndpoints(final String endpoint) throws Exception {
    mvcMock.perform(MockMvcRequestBuilders.get(endpoint).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(500))
        .andExpect(content().string(
            "{\"requestId\":\"main\",\"data\":null,\"error\":{\"code\":\"err-g-1\",\"message\":\"Internal Server Error.\",\"type\":\"ERROR\",\"helpUri\":null,\"context\":[]},\"messages\":[]}"));
  }


  static Stream<Arguments> dataEndpoints() {
    return Stream.of(
        Arguments.of("/voidHandler", "{\"requestId\":\"main\",\"data\":null,\"error\":null,\"messages\":[]}"),
        Arguments.of("/intHandler", "{\"requestId\":\"main\",\"data\":1,\"error\":null,\"messages\":[]}"),
        Arguments.of("/byteHandler", "{\"requestId\":\"main\",\"data\":127,\"error\":null,\"messages\":[]}"),
        Arguments.of("/shortHandler", "{\"requestId\":\"main\",\"data\":32767,\"error\":null,\"messages\":[]}"),
        Arguments.of("/longHandler",
            "{\"requestId\":\"main\",\"data\":9223372036854775807,\"error\":null,\"messages\":[]}"),
        Arguments.of("/floatHandler", "{\"requestId\":\"main\",\"data\":3.14,\"error\":null,\"messages\":[]}"),
        Arguments.of("/doubleHandler", "{\"requestId\":\"main\",\"data\":3.14159,\"error\":null,\"messages\":[]}"),
        Arguments.of("/booleanHandler", "{\"requestId\":\"main\",\"data\":true,\"error\":null,\"messages\":[]}"),
        Arguments.of("/charHandler", "{\"requestId\":\"main\",\"data\":\"a\",\"error\":null,\"messages\":[]}"),
        Arguments.of("/stringHandler", "{\"requestId\":\"main\",\"data\":\"string\",\"error\":null,\"messages\":[]}"),

        Arguments.of("/responseVoidHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":null,\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseStringHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":\"string\",\"error\":null,\"messages\":[]}"),

        Arguments.of("/responseVoidArrayHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":[],\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseObjectArrayHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":[],\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseStringArrayHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":[],\"error\":null,\"messages\":[]}"),

        Arguments.of("/responseEntityVoidHandler",
            "{\"requestId\":\"main\",\"data\":null,\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseEntityStringHandler",
            "{\"requestId\":\"main\",\"data\":\"string\",\"error\":null,\"messages\":[]}"),

        Arguments.of("/responseEntityResponseVoidHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":null,\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseEntityResponseStringHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":\"string\",\"error\":null,\"messages\":[]}"),

        Arguments.of("/responseEntityVoidArrayHandler",
            "{\"requestId\":\"main\",\"data\":[],\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseEntityObjectArrayHandler",
            "{\"requestId\":\"main\",\"data\":[],\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseEntityStringArrayHandler",
            "{\"requestId\":\"main\",\"data\":[\"string\"],\"error\":null,\"messages\":[]}"),

        Arguments.of("/responseEntityResponseVoidArrayHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":[],\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseEntityResponseObjectArrayHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":[],\"error\":null,\"messages\":[]}"),
        Arguments.of("/responseEntityResponseStringArrayHandler",
            "{\"requestId\":\"controller-request-id\",\"data\":[],\"error\":null,\"messages\":[]}")
    );
  }

  static Stream<String> exceptionEndpoints() {
    return Stream.of(
        "/voidHandlerException",
        "/intHandlerException",
        "/byteHandlerException",
        "/shortHandlerException",
        "/longHandlerException",
        "/floatHandlerException",
        "/doubleHandlerException",
        "/booleanHandlerException",
        "/charHandlerException",
        "/objectHandlerException",
        "/stringHandlerException",
        "/responseVoidHandlerException",
        "/responseObjectHandlerException",
        "/responseStringHandlerException",
        "/responseEntityVoidHandlerException",
        "/responseEntityObjectHandlerException",
        "/responseEntityStringHandlerException",
        "/responseEntityResponseVoidHandlerException",
        "/responseEntityResponseObjectHandlerException",
        "/responseEntityResponseStringHandlerException",
        "/responseVoidArrayHandlerException",
        "/responseObjectArrayHandlerException",
        "/responseStringArrayHandlerException",
        "/responseEntityVoidArrayHandlerException",
        "/responseEntityObjectArrayHandlerException",
        "/responseEntityStringArrayHandlerException",
        "/responseEntityResponseVoidArrayHandlerException",
        "/responseEntityResponseObjectArrayHandlerException",
        "/responseEntityResponseStringArrayHandlerException"
    );
  }

  @RestController
  public static class TestingController {

    @SuppressWarnings("EmptyMethod")
    @GetMapping("/voidHandler")
    public void voidHandler() {
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/intHandler")
    public int intHandler() {
      return 1;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/byteHandler")
    public byte byteHandler() {
      return 127;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/shortHandler")
    public short shortHandler() {
      return 32767;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/longHandler")
    public long longHandler() {
      return 9223372036854775807L;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/floatHandler")
    public float floatHandler() {
      return 3.14f;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/doubleHandler")
    public double doubleHandler() {
      return 3.14159;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/booleanHandler")
    public boolean isBooleanHandler() {
      return true;
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/charHandler")
    public char charHandler() {
      return 'a';
    }

    @SuppressWarnings("SameReturnValue")
    @GetMapping("/stringHandler")
    public String stringHandler() {
      return "string";
    }

    // Response Endpoints
    @GetMapping("/responseVoidHandler")
    public Response<Void> responseVoidHandler() {
      return new ResponseDto<>("controller-request-id", (Void) null, Collections.emptyList()
      );
    }

    @GetMapping("/responseStringHandler")
    public Response<String> responseStringHandler() {
      return new ResponseDto<>("controller-request-id", "string", Collections.emptyList());
    }

    // ResponseEntity Endpoints
    @GetMapping("/responseEntityVoidHandler")
    public ResponseEntity<Void> responseEntityVoidHandler() {
      return ResponseEntity.ok(null);
    }

    @GetMapping("/responseEntityStringHandler")
    public ResponseEntity<String> responseEntityStringHandler() {
      return ResponseEntity.ok("string");
    }

    // ResponseEntity<Response> Endpoints
    @GetMapping("/responseEntityResponseVoidHandler")
    public ResponseEntity<Response<Void>> responseEntityResponseVoidHandler() {
      return ResponseEntity.ok(
          new ResponseDto<>("controller-request-id", (Void) null, Collections.emptyList()
          ));
    }

    @GetMapping("/responseEntityResponseStringHandler")
    public ResponseEntity<Response<String>> responseEntityResponseStringHandler() {
      return ResponseEntity.ok(new ResponseDto<>("controller-request-id", "string",
          Collections.emptyList()));
    }

    // Response Array Endpoints
    @GetMapping("/responseVoidArrayHandler")
    public Response<Void[]> responseVoidArrayHandler() {
      return new ResponseDto<>("controller-request-id", new Void[]{}, Collections.emptyList()
      );
    }

    @GetMapping("/responseObjectArrayHandler")
    public Response<Object[]> responseObjectArrayHandler() {
      return new ResponseDto<>("controller-request-id", new Object[]{}, Collections.emptyList());
    }

    @GetMapping("/responseStringArrayHandler")
    public Response<String[]> responseStringArrayHandler() {
      return new ResponseDto<>("controller-request-id", new String[]{}, Collections.emptyList());
    }

    // ResponseEntity Array Endpoints
    @GetMapping("/responseEntityVoidArrayHandler")
    public ResponseEntity<Void[]> responseEntityVoidArrayHandler() {
      return ResponseEntity.ok(new Void[]{});
    }

    @GetMapping("/responseEntityObjectArrayHandler")
    public ResponseEntity<Object[]> responseEntityObjectArrayHandler() {
      return ResponseEntity.ok(new Object[]{});
    }

    @GetMapping("/responseEntityStringArrayHandler")
    public ResponseEntity<String[]> responseEntityStringArrayHandler() {
      return ResponseEntity.ok(new String[]{"string"});
    }

    // ResponseEntity<Response> Array Endpoints
    @GetMapping("/responseEntityResponseVoidArrayHandler")
    public ResponseEntity<Response<Void[]>> responseEntityResponseVoidArrayHandler() {
      return ResponseEntity.ok(
          new ResponseDto<>("controller-request-id", new Void[]{}, Collections.emptyList()
          ));
    }

    @GetMapping("/responseEntityResponseObjectArrayHandler")
    public ResponseEntity<Response<Object[]>> responseEntityResponseObjectArrayHandler() {
      return ResponseEntity.ok(new ResponseDto<>("controller-request-id", new Object[]{},
          Collections.emptyList()));
    }

    @GetMapping("/responseEntityResponseStringArrayHandler")
    public ResponseEntity<Response<String[]>> responseEntityResponseStringArrayHandler() {
      return ResponseEntity.ok(new ResponseDto<>("controller-request-id", new String[]{},
          Collections.emptyList()));
    }
  }

  @RestController
  public static class TestingExceptionController {

    @GetMapping("/voidHandlerException")
    public void voidHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/intHandlerException")
    public int intHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/byteHandlerException")
    public byte byteHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/shortHandlerException")
    public short shortHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/longHandlerException")
    public long longHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/floatHandlerException")
    public float floatHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/doubleHandlerException")
    public double doubleHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/booleanHandlerException")
    public boolean isBooleanHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/charHandlerException")
    public char charHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/objectHandlerException")
    public Object objectHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/stringHandlerException")
    public String stringHandlerException() {
      throw new RuntimeException();
    }

    // Response Endpoints
    @GetMapping("/responseVoidHandlerException")
    public Response<Void> responseVoidHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseObjectHandlerException")
    public Response<Object> responseObjectHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseStringHandlerException")
    public Response<String> responseStringHandlerException() {
      throw new RuntimeException();
    }

    // ResponseEntity Endpoints
    @GetMapping("/responseEntityVoidHandlerException")
    public ResponseEntity<Void> responseEntityVoidHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityObjectHandlerException")
    public ResponseEntity<Object> responseEntityObjectHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityStringHandlerException")
    public ResponseEntity<String> responseEntityStringHandlerException() {
      throw new RuntimeException();
    }

    // ResponseEntity<Response> Endpoints
    @GetMapping("/responseEntityResponseVoidHandlerException")
    public ResponseEntity<Response<Void>> responseEntityResponseVoidHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityResponseObjectHandlerException")
    public ResponseEntity<Response<Object>> responseEntityResponseObjectHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityResponseStringHandlerException")
    public ResponseEntity<Response<String>> responseEntityResponseStringHandlerException() {
      throw new RuntimeException();
    }

    // Response Array Endpoints
    @GetMapping("/responseVoidArrayHandlerException")
    public Response<Void[]> responseVoidArrayHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseObjectArrayHandlerException")
    public Response<Object[]> responseObjectArrayHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseStringArrayHandlerException")
    public Response<String[]> responseStringArrayHandlerException() {
      throw new RuntimeException();
    }

    // ResponseEntity Array Endpoints
    @GetMapping("/responseEntityVoidArrayHandlerException")
    public ResponseEntity<Void[]> responseEntityVoidArrayHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityObjectArrayHandlerException")
    public ResponseEntity<Object[]> responseEntityObjectArrayHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityStringArrayHandlerException")
    public ResponseEntity<String[]> responseEntityStringArrayHandlerException() {
      throw new RuntimeException();
    }

    // ResponseEntity<Response> Array Endpoints
    @GetMapping("/responseEntityResponseVoidArrayHandlerException")
    public ResponseEntity<Response<Void[]>> responseEntityResponseVoidArrayHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityResponseObjectArrayHandlerException")
    public ResponseEntity<Response<Object[]>> responseEntityResponseObjectArrayHandlerException() {
      throw new RuntimeException();
    }

    @GetMapping("/responseEntityResponseStringArrayHandlerException")
    public ResponseEntity<Response<String[]>> responseEntityResponseStringArrayHandlerException() {
      throw new RuntimeException();
    }
  }

  @ControllerAdvice
  private class TestSpringWebMvcRequestExceptionResolverControllerAdvice implements
      GenericExceptionHandler<String> {

    private final ThreadLocalStorage storage = new ThreadLocalStorage(pid);

    @Override
    public LogMapper getLogMapper() {
      return mapper.getLogMapper();
    }
    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return mapper;
    }

    @Override
    public MessageStorage getMessageStorage() {
      return storage;
    }
  }

  private static class TestHttpResponseMapper implements ResponseEntityResponseDataMapper {

    private final BaseResponseMapper mapper;
    private final ThreadLocalStorage storage;

    public TestHttpResponseMapper(final BaseResponseMapper mapper) {
      this.mapper = mapper;
      this.storage = new ThreadLocalStorage(() -> "thread-id-provider");
    }

    @Override
    public LogMapper getLogMapper() {
      return mapper.getLogMapper();
    }

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return this.mapper;
    }

    @Override
    public MessageStorage getMessageStorage() {
      return this.storage;
    }
  }
}

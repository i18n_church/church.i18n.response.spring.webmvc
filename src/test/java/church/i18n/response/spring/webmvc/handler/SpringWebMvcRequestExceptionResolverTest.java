/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.webmvc.handler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.BaseResponseMapper;
import church.i18n.response.mapper.ResponseMapper;
import church.i18n.response.spring.mapper.ResponseEntityResponseDataMapper;
import org.junit.jupiter.api.Test;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

class SpringWebMvcRequestExceptionResolverTest {

  private static final MockMvc mvcMock = MockMvcBuilders
      .standaloneSetup(new TestingController())
      .setControllerAdvice(new TestSpringWebMvcRequestExceptionResolverControllerAdvice())
      .setMessageConverters(new ResponseDtoToStringHttpMessageConverter())
      .build();

  @Test
  void handleMethodArgumentNotValidException() throws Exception {
    final MockHttpServletResponse servletResponse = mvcMock
        .perform(MockMvcRequestBuilders.get("/BindExceptionMethodArgumentNotValidException"))
        .andReturn()
        .getResponse();
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
            + "code='err-wr-3', message='No handler found for this HTTP method: \"GET\" and given "
            + "endpoint: \"/BindExceptionMethodArgumentNotValidException\".', type=ERROR, "
            + "helpUri='null', context=[ContextInfoDto{name='method', value=GET, "
            + "valueType='string', help=null, helpType='null', message='null'}, "
            + "ContextInfoDto{name='url', value=/BindExceptionMethodArgumentNotValidException, "
            + "valueType='string', help=null, helpType='null', message='null'}]}, messages=[]}",
        servletResponse.getContentAsString());
  }

  @Test
  void handleMethodArgumentNotValidExceptionWithFieldError() throws Exception {
    final MockHttpServletResponse servletResponse = mvcMock
        .perform(MockMvcRequestBuilders.get("/MethodArgumentNotValidExceptionWithFieldError"))
        .andReturn()
        .getResponse();
    assertEquals("ResponseDto{requestId='main', data=null, error=ResponseMessageDto{"
            + "code='err-wr-2', message='Validation has failed.', type=ERROR, helpUri='null', "
            + "context=[ContextInfoDto{name='objectName', value=null, valueType='null', "
            + "help=Codes: null Arguments: null, helpType='null', message='default message'}, "
            + "ContextInfoDto{name='String', value=null, valueType='null', help=Codes: "
            + "[help1, help2] Arguments: [3.141, asd, 1], helpType='null', "
            + "message='Object validation failed.'}, ContextInfoDto{name='field', value=null, "
            + "valueType='null', help=Codes: null Arguments: null, helpType='null', "
            + "message='default message'}]}, messages=[]}",
        servletResponse.getContentAsString());
  }

  @Test
  void handleNoHandlerFoundException() throws Exception {
    final MockHttpServletResponse servletResponse = mvcMock
        .perform(MockMvcRequestBuilders.get("/non-existing-handler"))
        .andReturn()
        .getResponse();

    assertEquals(
        "ResponseDto{requestId='main', data=null, error=ResponseMessageDto{code='err-wr-3', "
            + "message='No handler found for this HTTP method: \"GET\" and given endpoint: "
            + "\"/non-existing-handler\".', type=ERROR, helpUri='null', context=[ContextInfoDto"
            + "{name='method', value=GET, valueType='string', help=null, helpType='null', "
            + "message='null'}, ContextInfoDto{name='url', value=/non-existing-handler, "
            + "valueType='string', help=null, helpType='null', message='null'}]}, messages=[]}",
        servletResponse.getContentAsString());
    assertEquals(HttpStatus.NOT_FOUND.value(), servletResponse.getStatus());
  }

  @SuppressWarnings("EmptyMethod")
  void handle(final String arg) {
    //testing method
  }

  @RestController
  private static class TestingController {

    @GetMapping("/MethodArgumentNotValidException")
    public ResponseEntity<String> handleMethodArgumentNotValidException() throws Exception {
      throw new MethodArgumentNotValidException(
          new MethodParameter(
              SpringWebMvcRequestExceptionResolverTest.class
                  .getDeclaredMethod("handle", String.class),
              0),
          new BeanPropertyBindingResult(new Param(), "param")
      );
    }

    @GetMapping("/MethodArgumentNotValidExceptionWithFieldError")
    public ResponseEntity<String> handleMethodArgumentNotValidExceptionWithFieldError()
        throws Exception {
      FieldError fieldError = new FieldError("objectName", "field", "default message");
      ObjectError objectError = new ObjectError("String", new String[]{"help1", "help2"},
          new Object[]{3.141, "asd", 1L}, "Object validation failed."
      );
      BeanPropertyBindingResult bindingResultWithFieldError = new BeanPropertyBindingResult(
          new Param(), "param");
      bindingResultWithFieldError.addError(fieldError);
      bindingResultWithFieldError.addError(objectError);

      throw new MethodArgumentNotValidException(
          new MethodParameter(
              SpringWebMvcRequestExceptionResolverTest.class
                  .getDeclaredMethod("handle", String.class),
              0),
          bindingResultWithFieldError
      );
    }
  }

  @ControllerAdvice
  private static class TestSpringWebMvcRequestExceptionResolverControllerAdvice implements
      SpringWebMvcRequestExceptionResolver<String> {

    private final ProcessingIdProvider pid = () -> "main";
    final BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withBundle(new PolyglotMultiResourceBundle(
            SpringWebMvcRequestExceptionResolver.RESOURCE_BUNDLE_NAME))
        .withExposeSanitizer((infoSecurityPolicy, config) -> true)
        .withProcessingIdProvider(pid)
        .build();
    final ResponseEntityResponseDataMapper stringMapper = new TestHttpResponseMapper(
        this.mapper);
    private final ThreadLocalStorage storage = new ThreadLocalStorage(pid);

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return this.mapper;
    }

    @Override
    public LogMapper getLogMapper() {
      return mapper.getLogMapper();
    }

    @Override
    public MessageStorage getMessageStorage() {
      return this.storage;
    }
  }

  private static class TestHttpResponseMapper implements ResponseEntityResponseDataMapper {

    private final BaseResponseMapper mapper;
    private final ThreadLocalStorage storage;

    public TestHttpResponseMapper(final BaseResponseMapper mapper) {
      this.mapper = mapper;
      this.storage = new ThreadLocalStorage(() -> "thread-id-provider");
    }

    @Override
    public ResponseMapper<Response<?>> getMapper() {
      return this.mapper;
    }

    @Override
    public LogMapper getLogMapper() {
      return mapper.getLogMapper();
    }

    @Override
    public MessageStorage getMessageStorage() {
      return this.storage;
    }
  }

  private static class Param {

    private Long id;
  }
}

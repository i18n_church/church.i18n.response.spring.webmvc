module church.i18n.response.spring.webmvc {
  requires transitive org.jetbrains.annotations;
  requires transitive jakarta.servlet;
  requires static spring.web;
  requires static spring.webmvc;

  requires church.i18n.response.spring;
  requires church.i18n.processing.exception;
  requires spring.core;
  requires church.i18n.response;

  opens church.i18n.response.spring.webmvc.handler;
}
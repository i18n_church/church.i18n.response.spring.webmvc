/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.webmvc.handler;

import static church.i18n.processing.message.ValueType.STRING;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ProcessingMessageBuilder;
import church.i18n.response.spring.mapper.ResponseEntityExceptionMapper;
import church.i18n.response.status.HttpStatusCode;
import jakarta.servlet.http.HttpServletRequest;
import java.net.BindException;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * Handler that overrides default exception handler from the spring framework and mapping exceptions to {@link ProcessingException}.
 *
 * @param <R> Type of the endpoint response.
 */
public interface SpringWebMvcRequestExceptionResolver<R> extends ResponseEntityExceptionMapper<R> {

  /**
   * Resource bundle consisting of public messages.
   */
  @NotNull
  String RESOURCE_BUNDLE_NAME = "church.i18n.response.spring.webmvc.handler.errors-spring-webmvc-request-handler";

  /**
   * Exception handler for {@link BindException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(BindException.class)
  default @NotNull ResponseEntity<R> handleBindException(final @NotNull BindException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest) {
    ProcessingException processingException = new ProcessingException("err-wr-1", exception)
        .withSecurityLevel(SYSTEM_INTERNAL)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(processingException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link MethodArgumentNotValidException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  default @NotNull ResponseEntity<R> handleMethodArgumentNotValidException(
      final @NotNull MethodArgumentNotValidException exception,
      final @NotNull HttpServletRequest servletRequest, final @NotNull WebRequest webRequest) {
    final ProcessingMessageBuilder<ProcessingMessage> message = ProcessingMessage
        .withMessage("err-wr-2")
        .withSecurityLevel(SYSTEM_EXTERNAL);

    if (exception.getBindingResult().hasGlobalErrors()) {
      exception.getBindingResult().getAllErrors().forEach(
          e -> message.addContextInfo(
              ContextInfo.of(e.getObjectName())
                  .withHelp("Codes: " + Arrays.toString(e.getCodes()) +
                      " Arguments: " + Arrays.toString(e.getArguments()))
                  .withMessage(e.getDefaultMessage())
                  .withSecurityLevel(SYSTEM_INTERNAL)
                  .build()
          ));
    }

    if (exception.getBindingResult().hasFieldErrors()) {
      exception.getBindingResult().getFieldErrors().forEach(
          e -> message.addContextInfo(
              ContextInfo.of(e.getField())
                  .withContext(String.valueOf(e.getRejectedValue()))
                  .withHelp("Codes: " + Arrays.toString(e.getCodes()) +
                      " Arguments: " + Arrays.toString(e.getArguments()))
                  .withMessage(e.getDefaultMessage())
                  .withSecurityLevel(e.isBindingFailure() ? SYSTEM_INTERNAL : SYSTEM_EXTERNAL)
                  .build()
          ));
    }
    ProcessingException responseException = new ProcessingException(message.build(), exception)
        .withStatus(HttpStatusCode.BAD_REQUEST_400);
    return error(responseException, servletRequest, webRequest);
  }

  /**
   * Exception handler for {@link NoHandlerFoundException} exception.
   *
   * @param exception      Exception that will be processed.
   * @param servletRequest HTTP servlet request.
   * @param webRequest     General request metadata.
   * @return Exception mapped to the response.
   */
  @ExceptionHandler(NoHandlerFoundException.class)
  default @NotNull ResponseEntity<R> handleNoHandlerFoundException(
      final @NotNull NoHandlerFoundException exception,
      final @NotNull HttpServletRequest servletRequest,
      final @NotNull WebRequest webRequest) {
    ProcessingMessage message = ProcessingMessage
        .withMessage("err-wr-3", exception.getHttpMethod(), exception.getRequestURL())
        .addContextInfo(
            ContextInfo.of("method").withContext(exception.getHttpMethod(), STRING).build(),
            ContextInfo.of("url").withContext(exception.getRequestURL(), STRING).build()
        )
        .withSecurityLevel(SYSTEM_EXTERNAL)
        .build();

    ProcessingException processingException = new ProcessingException(message, exception)
        .withStatus(HttpStatusCode.NOT_FOUND_404);
    return error(processingException, servletRequest, webRequest);
  }

}

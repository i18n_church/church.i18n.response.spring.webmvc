/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.spring.webmvc.handler;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.response.domain.Response;
import church.i18n.response.mapper.ResponseMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@RestControllerAdvice
public class ResponseMappingAdvice implements ResponseBodyAdvice<Object> {

  private final ResponseMapper<?> mapper;
  private final String[] excludePatterns;
  private final AntPathMatcher antPathMatcher = new AntPathMatcher();

  public ResponseMappingAdvice(
      final @NotNull ResponseMapper<?> mapper,
      final @Nullable String... excludePatterns
  ) {
    this.mapper = mapper;
    this.excludePatterns = (excludePatterns == null) ? new String[0] : excludePatterns;
  }

  @Override
  public boolean supports(final MethodParameter returnType,
      final Class<? extends HttpMessageConverter<?>> converterType) {

    // Return true if you want to modify the response for this type of controller method
    // In general this support any kind of object. In before write it handles 3 different types of responses.
    //If method returns
    // 1) Response or ResponseWrapper<Response> already mapped through ResponseMapper, we just forward it.
    // 2) ResponseWrapper that wraps any object will be wrapped in Response structure
    // 3) Any other object will be wrapped in Response structure.
    // For example, you might check for specific annotations or return types here
    return true;
  }

  @Override
  public Object beforeBodyWrite(
      final @Nullable Object body,
      final MethodParameter returnType,
      final MediaType selectedContentType,
      final Class<? extends HttpMessageConverter<?>> selectedConverterType,
      final ServerHttpRequest request,
      final ServerHttpResponse response) {
    String requestUrl = request.getURI().getRawPath();
    //If we want to exclude specific paths from remapping.
    for (String excludePattern : excludePatterns) {
      if (antPathMatcher.match(excludePattern, requestUrl)) {
        return body;
      }
    }

    return (body instanceof Response)
           ? body
           : mapper.map(body, (ProcessingException) null, request.getHeaders().getAcceptLanguageAsLocales());
  }
}


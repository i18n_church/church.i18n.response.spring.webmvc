/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

plugins {
    id("church.18n.public.java-project")
}

description = """Extension of `church.i18n.response.spring` library for Spring Web MVC projects. It 
adds handling of most common Spring WebMVC Exceptions. """

//Plugin is applied only after description is set, otherwise it's missing.
apply(plugin = "church.18n.public.java-project-publishable")

dependencies {
    api(project(":response.spring"))
    api(platform(project(":spring.webmvc.platform")))

    api(libs.slf4j.api)
    api(libs.spring.webmvc)
    api(libs.spring.core)
    api(libs.servlet.api)

    implementation(libs.jetbrains.annotations)

    testImplementation(project(":response.model.jackson"))
    testImplementation(libs.bundles.jackson.base)

    testImplementation(libs.hamcrest)
    testImplementation(libs.archunit)
    testImplementation(libs.bundles.junit)
    testImplementation(libs.validation.api)
    testImplementation(libs.bundles.spring.test)
}
